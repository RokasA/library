<?php
/**
 * Created by PhpStorm.
 * User: rokas
 * Date: 8/17/2018
 * Time: 11:09 AM
 */

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class LibraryController extends Controller
{
    /**
     * @Route("/library", name="library_list")
     */
    public function listAction(Request $request)
    {

        return $this->render('library/basic.html.twig');
    }



}