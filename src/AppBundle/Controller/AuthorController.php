<?php
/**
 * Created by PhpStorm.
 * User: rokas
 * Date: 8/22/2018
 * Time: 1:53 PM
 */
/**
 * Created by PhpStorm.
 * User: rokas
 * Date: 8/17/2018
 * Time: 11:09 AM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Library\Author;
use AppBundle\Entity\Library\Book;
use AppBundle\Repository\AuthorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Form\AuthorType;

class AuthorController extends Controller
{

    /**
     * @Route("/library/author", name="library_author_list")
     */

    public function listAuthorAction(Request $request)
    {

        $data = $request->request->get('search');
        if ($request->isMethod('post') && $data != '') {
           $authors = $this->getDoctrine()->getRepository(Author::class)
               ->filterAuthorsByName($data);
        }
        else{
            $authors = $this->getDoctrine()->getRepository('AppBundle:Library\Author')->findAll();
        }



        return $this->render('library/authorlist.html.twig',array('authors' => $authors));
    }

    /**
     * @Route("/library/author/create", name = "library_author_create")
     */
    public function createAuthorAction(Request $request)
    {
        $author = new Author();
        $form = $this->createForm(AuthorType::class, $author);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('library_author_list'));
        }

        return $this->render('library/authorcreate.html.twig',array('form' => $form->createView()));
    }

    /**
     * @Route("/library/author/edit/{id}", name = "library_author_edit")
     */
    public function editAuthorAction($id, Request $request)
    {
        $author = $this->getDoctrine()->getRepository("AppBundle:Library\Author")->find($id);
        $form = $this->createForm(AuthorType::class, $author);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($author);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('library_author_details',array("id" =>$id)));
        }
        return $this->render('library/authoredit.html.twig',array('form' => $form->createView()));
    }

    /**
     * @Route("/library/author/details/{id}", name = "library_author_details")
     */
    public function detailsAuthorAction($id, Request $request)
    {
        $author = $this->getDoctrine()->getRepository("AppBundle:Library\Author")->find($id);
        $books = $this->getDoctrine()->getRepository("AppBundle:Library\Book")->findAll();

        return $this->render('library/authordetails.html.twig',array('author' => $author,'books' => $books));
    }

    /**
     * @Route("/library/author/delete/{id}", name = "library_author_delete")
     */
    public function deleteauthorAction($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $author = $entityManager->getRepository('AppBundle:Library\Author')->find($id);
        $entityManager->remove($author);
        $entityManager->flush();

        $authors = $this->getDoctrine()->getRepository("AppBundle:Library\Author")->findAll();


        return $this->render('library/authorlist.html.twig', array("authors" => $authors));
    }


}