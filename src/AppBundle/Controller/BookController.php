<?php
/**
 * Created by PhpStorm.
 * User: rokas
 * Date: 8/22/2018
 * Time: 1:56 PM
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Library\Book;
use AppBundle\Entity\Library\AuthorRepository;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use AppBundle\Form\BookType;
use AppBundle\Repository\BookRepository;

class BookController extends Controller
{

    /**
     * @Route("/library/book", name="library_book_list")
     */
    public function listBookAction(Request $request)
    {
        $data = $request->request->get('search');
        if ($request->isMethod('post') && $data != '') {
            $books = $this->getDoctrine()->getRepository(Book::class)
                ->filterByAuthorNameAndIsbn($data);
        }
        else{
            $books = $this->getDoctrine()->getRepository('AppBundle:Library\Book')->findAll();
        }
        return $this->render('library/booklist.html.twig',array('books' => $books));
    }

    /**
     * @Route("/library/book/create", name = "library_book_create")
     */
    public function createbookAction(Request $request)
    {
        $book = new Book();
        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('library_book_list'));
        }

        return $this->render('library/bookcreate.html.twig',array('form' => $form->createView()));
    }


    /**
     * @Route("/library/book/edit/{id}", name = "library_book_edit")
     */
    public function editbookAction($id, Request $request)
    {
        $book = $this->getDoctrine()->getRepository("AppBundle:Library\Book")->find($id);
        $form = $this->createForm(BookType::class, $book);

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($book);
            $entityManager->flush();

            return $this->redirect($this->generateUrl('library_book_details',array("id" => $id)));
        }

        return $this->render('library/bookedit.html.twig',array('form' => $form->createView()));
    }

    /**
     * @Route("/library/book/details/{id}", name = "library_book_details")
     */
    public function detailsbookAction($id, Request $request)
    {
        $book = $this->getDoctrine()->getRepository('AppBundle:Library\Book')->find($id);


        return $this->render('library/bookdetails.html.twig', array("id" =>$id, "book" => $book));
    }

    /**
     * @Route("/library/book/delete/{id}", name = "library_book_delete")
     */
    public function deletebookAction($id, Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $book = $entityManager->getRepository('AppBundle:Library\Book')->find($id);
        $entityManager->remove($book);
        $entityManager->flush();

        $books = $this->getDoctrine()->getRepository("AppBundle:Library\Book")->findAll();


        return $this->render('library/booklist.html.twig', array("books" => $books));
    }


}