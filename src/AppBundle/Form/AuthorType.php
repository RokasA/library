<?php
/**
 * Created by PhpStorm.
 * User: rokas
 * Date: 8/20/2018
 * Time: 9:41 AM
 */
namespace AppBundle\Form;

use AppBundle\Entity\Library\Book;
use AppBundle\Entity\Library\Author;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

class AuthorType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('fullname', TextType::class, array('label' => 'Full name of the author: ','attr' => array('class' => 'form-control','style' => 'margin-bottom:10px')));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Author::class,
        ));
    }
}