<?php
/**
 * Created by PhpStorm.
 * User: rokas
 * Date: 8/20/2018
 * Time: 11:49 AM
 */

namespace AppBundle\Form;

use AppBundle\Entity\Library\Book;
use AppBundle\Entity\Library\Author;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;



class BookType extends AbstractType{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('price',NumberType::class,array('label' => 'Book price, Eur.:','scale' => 2, 'attr' => array('class' => 'form-control','style' => 'margin-bottom:10px')))
            ->add('isbn', TextType::class, array('label' => "Isbn number: ", 'attr' => array('class' => 'form-control','style' => 'margin-bottom:10px')))
            ->add('title', TextType::class, array('label' => 'Title of the book: ', 'attr' => array('class' => 'form-control','style' => 'margin-bottom:10px')))
            ->add('category', ChoiceType::class, array('label' => 'Category: ','choices' => array(
                    "Fantasy" => "Fantasy",
                    "Thriller" => "Thriller",
                    "Detective" => "Detective",
                    "Sci-fi" => "Sci-fi",
                    "Drama" => "Drama",
                    "Fiction" => "Fiction",
                    "Novel" => "Novel",
                    "Horror" => "Horror"
            ), 'attr' => array('class' => 'form-control','style' => 'margin-bottom:10px')))
            ->add('bookAuthors', EntityType::class, array('class' => Author::class,'choice_label' => 'fullName', 'multiple' => true, 'attr' => array('class' => 'form-control','style' => 'margin-bottom:10px')))

        ;
    }





    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Book::class,
        ));
    }
}